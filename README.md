## Takna: A better way to manage our time

## USR Features

####Activity Clock  

- Visual representation of activities (Work, Life, Sleep, Eat)  

- Reminds you of your activities  

- Prompts you if you have completed the activity on time.  

- Detailed view for all activities.  

  
####Activity List  

- List Representation of activities.  

- Add, update and cancel activities.  
  
####Work, Life Balance Bar  

- Ratio representation of Work & Life points  

- Notifies the user and the manager in charge that the user is overworked once the work bar goes above the maximum threshold.  
  
####LiveIT Store  

- Store used to purchase items using the earned LiveIT points.  
  
####Chatbox  

- Chatbox to communicate with team members.  
  
## MGR Features
  
####Manager has all USR Features.  
  
###Manager exclusive features:  
  
####Dashboard to view team's activity clocks  

- Managers are able to see all team members activity clock  

- This will help the manager Gauge the team's workload  

- Detail view for activities is only available for work activities, life activities are shown as 'Life Activity'.  

  Eg.  
  
      UserA's View of his/her Work Activity -> [Task #4321: Bug Fix for Infinite Loop]  
      Manager View of UserA's Work Activity -> [Task #4321: Bug Fix for Infinite Loop]  
  
      UserA's View of his/her Life Activity -> [Jogging with friends]  
      Manager View of UserA's Life Activity -> [Life Activity]  
  
- Notifies the manager that the user is overworked once the work bar goes above the maximum threshold.  
  
####Work activity assignment  

- Manager is able to create a list of Activities and task for the day and assign it to different members.  
  
####Team's Work Life points Statistics  

- Manager will be able to see on a Chart the life and work points of the whole team to better visualize the overall productivity.  
  
  
## Work, Life, and LiveIT points  
  
- Each Activity has a corresponding amount of Work/Life points depending on the duration of the activity.  

- Completing work/life activities ahead of schedule will reduce the amount of work/life points awarded to the user.  

- Extending work/life activities will increase the amount of work/life points awarded to the user.  

- Completing work/life activities on time will award the user with additional LiveIT points since he/she was able to gauge and manage his/her time properly.  

- Every week the work/life Balance Bar is reset and depending on the ratio of work and life points the user will be awarded with certain number of LiveIT points.  

  Eg.  

      UserA Work Points 7610 for Week 1  
      UserA Life Points 6342 for Week 1  
  
      Ratio is 1:1.2 (0.2) Difference  
  
      UserA is awarded 80 LiveIT points for Week 1  
  
      UserA Work Points 8107 for Week 2  
      UserA Life Points 4504 for Week 2  
  
      Ratio is 1:1.8 (0.8) Difference  
  
      UserA is awarded 20 LiveIT points for Week 2  
  

- LiveIT points can be used to buy items in the LiveIT shop.  
  
