import React, { Component } from 'react';
import { Provider } from 'react-redux';
import store from './store';
import ActivityList from './components/ActivityList';
import Clock from './components/Clock';
import Sidebar from './components/Sidebar';
import CustomChart from './components/CustomChart'
import './App.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faLeaf, faMoon, faBriefcase, faUtensils, faPenSquare, faMinusSquare } from '@fortawesome/free-solid-svg-icons'

library.add(faLeaf, faMoon, faBriefcase, faUtensils, faPenSquare, faMinusSquare)

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <div className="App">
          <Sidebar/>
          <Clock/>
          <ActivityList/>
          <CustomChart/>
        </div>
      </Provider>
    );
  }
}

export default App;
