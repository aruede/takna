import { FETCH_ACTIVITIES, NEW_ACTIVITY, UPDATE_ACTIVITY, DELETE_ACTIVITY } from './types';
import { ActivitiesRef } from '../../config/firebase'

export const fetchActivities = () => async dispatch => {
    ActivitiesRef.on('value', snapshot => {
        let activities = snapshot.val();
        let activitiesArray = [];
        if(null !== activities) {
            let  keys = Object.keys(activities);
        

        for(let i = 0; i < keys.length; i++){
            let key = keys[i];
            let activity = {
                key: key,
                name: activities[key].name,
                start: activities[key].start,
                end: activities[key].end,
                description: activities[key].description,
                priority: activities[key].priority,
                creator: activities[key].creator,
                type: activities[key].type
            }
            
            activitiesArray.push(activity)
        }
        }

        dispatch({
            type: FETCH_ACTIVITIES,
            payload: activitiesArray
        });
    });
}

export const addActivity = (activity) => async dispatch => {
    return ActivitiesRef.push().set(activity).catch( error => ({
        errorCode: error.code,
        errorMessage: error.message
    }));
}

export const updateActivity = () => async dispatch => {
    let activity = {
        activityName: 'renz',
        type: 'hack'
    }
    return ActivitiesRef.child('-LQu82rjoCIS5pr7ILw2').update(activity)
    .then(() => ActivitiesRef.once('value'))
    .then(snapshot => snapshot.val())
    .catch( error => ({
        errorCode: error.code,
        errorMessage: error.message
    }));
}

export const deleteActivity = (key) => async dispatch => {
    return ActivitiesRef.child(key).remove()
    .catch( error => ({
        errorCode: error.code,
        errorMessage: error.message
    }));
}