import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { toast } from 'react-toastify';
import {deleteActivity} from '../actions/ActivityList/actions'
import 'react-toastify/dist/ReactToastify.css'
class Activity extends Component {
  constructor(props){
    super(props)

    this.deleteHandler = this.deleteHandler.bind(this)
  }

  iconSelector = (activityType) => {
    switch(activityType){
      case 'Work':
        return 'briefcase'
      case 'Life':
        return 'leaf'
      case 'Sleep':
        return 'moon'
      case 'Eat':
        return 'utensils'
      default:
    }
  }

  deleteHandler = (key) => {
    this.props.deleteActivity(key)
  }

  render() {
    return (
          // <span>{this.props.activity.start}</span>
          // <span>{this.props.activity.end}</span>
          // <span>{this.props.activity.description}</span>
          // <span>{this.props.activity.priority}</span>
          // <span>{this.props.activity.creator}</span>
          // <span>{this.props.activity.type}</span>
          <div key={this.props.key} className="activity-item" id="activityItem">
            <a className={'activity-icon-container ' + this.props.activity.type.toLowerCase()}><FontAwesomeIcon className='activity-icon' icon ={this.iconSelector(this.props.activity.type)}/></a>
            <div className="activity-text">
              <span className="activity-name">{this.props.activity.name}</span>
              <span className="activity-duration">{this.props.activity.start} - {this.props.activity.end}</span>
            </div>
            <div className="activity-actions">
            <a className='activity-icon-container'><FontAwesomeIcon className='edit-icon' icon ="pen-square"/></a>
            <a className='activity-icon-container' ><FontAwesomeIcon className='delete-icon' icon ="minus-square"/></a>
            </div>
          </div>
    )
  }
}

const mapStateToProps = (state) => ({

})

export default connect(mapStateToProps, { deleteActivity })(Activity);
