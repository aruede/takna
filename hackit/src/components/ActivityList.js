import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Dialog, Button, TextField } from '@material-ui/core'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { SimpleDialogWrapped } from './SimpleDialogWrapped'

//import DialogBox from './common/DialogBox'
import Activity from './Activity'
import { fetchActivities, addActivity, updateActivity, deleteActivity } from '../actions/ActivityList/actions'

class ActivityList extends Component {
    constructor(props){
        super(props)

        this.state = {
    
                name: 'HackIT',
                start: '6:00 PM',
                end: '6:00 PM',
                description: '',
                priority: '',
                creator: '',
                type: 'Life',

 
            modalIsOpen: false,
            title: 'Add Activity'
        }
    }
  
    componentWillMount = () => {
        this.props.fetchActivities()
    }
  
    componentDidUpdate = (prevProps) => {
        
    }

    openModal = () => {
        this.setState({
            modalIsOpen: true
        })
    }

    closeModal = () => {
        this.setState({
            modalIsOpen: false
        })
    }

    addActivityHandler = () => {
        let types = ['Work', 'Life', 'Eat', 'Sleep']
        let newType = types[Math.floor(Math.random()*types.length)];
        let newActivity = {
            name: this.state.name,
            start: this.state.start,
            end: this.state.end,
            description: this.state.description,
            priority: this.state.priority,
            creator: this.state.creator,
            type: newType
        }
        this.props.addActivity(newActivity)
        .then(response => {
            if(null !== response){
                if(undefined !== response && response.hasOwnProperty('errorMessage')){
                    toast.error('Add Failed!')
                }else{
                    toast.success('Add Success!')
                }
            }
        })
    }

    inputChangeHandler = (e) => {
        let inputData = e.target.value
        console.log(e)
        switch(e.target.name){
            case 'name':
                this.setState({
                    name: inputData
                });
                break;
            case 'start':
                this.setState({
                    start: inputData
                })
                break;
            case 'end':
                this.setState({
                    end: inputData
                })
                break;
            case 'description':
                this.setState({
                    description: inputData
                })
                break;
            case 'type':
                this.setState({
                    type: inputData
                })
                break;
            default:
        }
    }
    render() {
    return (
      
            
      
      <div>
      <span className="module-name">Activity List</span><br/><br/><br/>
      <div className='activity-list'>
      <Button variant="contained" color="primary" onClick={this.openModal}>
        Add
        </Button>
        {this.props.activities.map( (activity) => {
            return (<Activity key={activity.key} activity={activity}/>)
        })}
        
        <SimpleDialogWrapped
        {...this.state}
        open={this.state.modalIsOpen}
        onClose={this.closeModal}
        handleChange={this.inputChangeHandler}
        onSubmit={this.addActivityHandler}>
            
        </SimpleDialogWrapped>
        <ToastContainer
            position="bottom-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnVisibilityChange
            draggable
            pauseOnHover
            />
      </div>
      </div>
     
    )
  }
}

const mapStateToProps = (state) => ({
    activities: state.activities.activityList
})
export default connect(mapStateToProps, { fetchActivities, addActivity, updateActivity, deleteActivity })(ActivityList)