import React, { Component } from 'react'
import $ from "jquery"

class Sidebar extends Component {
  render() {
    return (
        <div class="sidebar">
        <div class="app-logo">
            <i class="app-icon fas fa-lemon"></i>  
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-home"></i></a>        
          <span class="nav-item-name">Home</span>
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-list-ul"></i></a>
          <span class="nav-item-name">List</span>
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-calendar-alt"></i></a>        
          <span class="nav-item-name">Calendar</span>
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-th-large"></i></a>        
          <span class="nav-item-name">Boxes</span>
        </div>
        <div class="nav-item">
        <a class="icon-container"><i class="nav-icon fas fa-chart-bar"></i></a>     
          <span class="nav-item-name">Graphs</span>
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-map"></i></a>        
          <span class="nav-item-name">Maps</span>
        </div>
        <div class="nav-item">
          <a class="icon-container"><i class="nav-icon fas fa-book-open"></i></a>        
          <span class="nav-item-name">Forms</span>
        </div>
        <div class="nav-item">
            <a class="icon-container"><i class="nav-icon fas fa-cog"></i></a>        
            <span class="nav-item-name">Settings</span>
        </div>
    </div>
    )
  }
  
}

export default Sidebar;