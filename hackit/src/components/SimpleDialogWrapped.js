import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { InputLabel, MenuItem, Select, Button, Dialog, DialogContent } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import blue from '@material-ui/core/colors/blue';

const emails = ['username@gmail.com', 'user02@gmail.com'];
const styles = {
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
};

class SimpleDialog extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      type: '',
      open: false
    }
  }
  
  handleSelectOpen = () => {
    this.setState({
      open:true
    })
  }

  handleSelectClose = () => {
    this.setState({
      open:true
    })
  }
  
  handleClose = () => {
    this.props.onClose(this.props.selectedValue);
  };

  handleListItemClick = value => {
    this.props.onClose(value);
  };

  onChange = e => {
    this.setState({
      type: e.target.value
    })
  }
  
  render() {
    const { classes, onClose, selectedValue, ...other } = this.props;

    return (
      <Dialog onClose={this.handleClose} aria-labelledby="simple-dialog-title" {...other}
      fullWidth={true}>
        <DialogTitle id="simple-dialog-title">{this.props.title}</DialogTitle>
        <DialogContent>
          <TextField
              autoFocus
              margin="dense"
              id="name"
              label="Activity Name"
              type="text"
              onChange={this.props.handleChange}
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="start"
              label="Start"
              type="time"
              onChange={this.props.handleChange}
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="end"
              label="End"
              type="time"
              onChange={this.props.handleChange}
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              id="description"
              label="Description"
              type="text"
              onChange={this.props.handleChange}
              fullWidth
            />
            
            <Button variant="contained" color="primary" onClick={this.props.onSubmit}>
            Submit
            </Button>

        </DialogContent>
      </Dialog>
    );
  }
}

SimpleDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  selectedValue: PropTypes.string,
};

export const SimpleDialogWrapped = withStyles(styles)(SimpleDialog);