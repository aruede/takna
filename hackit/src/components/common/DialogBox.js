import React, { Component } from 'react';
import { Modal, Button } from 'react-bootstrap';


class DialogBox extends Component {
  render() {
    return (
      <div>
        <Modal.Dialog show={this.props.showDialog}>
    <Modal.Header>
      <Modal.Title>Modal title</Modal.Title>
    </Modal.Header>

    <Modal.Body>One fine body...</Modal.Body>

    <Modal.Footer>
      <Button>Close</Button>
      <Button bsStyle="primary">Save changes</Button>
    </Modal.Footer>
  </Modal.Dialog>
      </div>
    )
  }
}

export default DialogBox;
