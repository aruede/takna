import React, { Component } from 'react'
import $ from "jquery"

class Clock extends Component {
  render() {
    return (
        <div className="clock-section" id="clock">
        <div className="clock-container">
          <img src={"../images/clock.png"} className="faux-clock"/>,
          {/* <div className="outer-circle"> */}
            <div id="liveclock" className="outer_face">
                <div className="marker oneseven"></div>
                <div className="marker twoeight"></div>
                <div className="marker fourten"></div>
                <div className="marker fiveeleven"></div>   
                <div className="inner_face">
                <div className="hand hour"></div>
              {/* </div> */}
              <div id="clockDiv"></div>
              <div id="clockTextDiv">Training in 80 minutes</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
  componentDidMount() {
    $(document).ready(function() {

        var $hands = $('#liveclock div.hand')
      
        window.requestAnimationFrame = window.requestAnimationFrame
                                       || window.mozRequestAnimationFrame
                                       || window.webkitRequestAnimationFrame
                                       || window.msRequestAnimationFrame
                                       || function(f){return setTimeout(f, 1000/60)}

        function updateclock(){
        var curdate = new Date()
        // var hour_as_degree = ( curdate.getSeconds() + curdate.getMilliseconds()/1000 ) /60 * 360
        var hour_as_degree = ( curdate.getHours() + curdate.getMinutes()/60 ) / 24 * 360
        $hands.filter('.hour').css({transform: 'rotate(' + hour_as_degree + 'deg)' })
        requestAnimationFrame(updateclock) //
        }
        
        requestAnimationFrame(updateclock)
      
        function displayTime() {
          
          //define a variable for Date() object to store date/time information   
          var time = new Date();
          
          //define variables to store hours, minutes, and seconds
          //use Date object methods, i.e., getHours, getMinutes, getSeconds, to store desired info  
          var hours = time.getHours();
          var minutes = time.getMinutes();
          var seconds = time.getSeconds();
          
          //for 12hour clock, define a variable meridiem and default to ante meridiem (AM) 
          var meridiem = " AM";
          
          //since this is a 12 hour clock, once hours increase past 11, i.e., 12 -23, subtract 12 and set the meridiem
          //variable to post meridiem (PM) 
          if (hours>11){
            hours = hours - 12;
            meridiem = " PM";
          }
          
          //at 12PM, the above if statement is set to subtract 12, making the hours read 0. 
          //create a statement that sets the hours back to 12 whenever it's 0.
          if (hours === 0){
            hours = 12;
          }
          
          //keep hours, seconds, and minutes at two digits all the time by adding a 0.
          if (hours<10){
            hours = "0" + hours;
          }
       
          if (minutes<10){
            minutes = "0" + minutes;
          }
          
          if (seconds<10){
            seconds = "0" + seconds;
          }
          
          //jquery to change text of clockDiv html element
          $("#clockDiv").text(hours +" : "+ minutes + meridiem);
          
          //could also write this with vanilla JS as follows
          //var clock = document.getElesmentById('clockDiv');
         //clock.innerText = hours +":"+ minutes +":"+ seconds + meridiem;
          
        }
        //run displayTime function
        displayTime();
        //set interval to 1000ms (1s), so that info updates every second
        setInterval(displayTime, 1000);
        });
  }
}

export default Clock;