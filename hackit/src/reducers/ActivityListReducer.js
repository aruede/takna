import { FETCH_ACTIVITIES, NEW_ACTIVITY, UPDATE_ACTIVITY } from '../actions/ActivityList/types';

const initialState = {
    activityList: [],
    newActivity: {}
}

export default (state = initialState, action) => {
    switch (action.type){
        case FETCH_ACTIVITIES:
            return {
                ...state,
                activityList: action.payload
            }
        case NEW_ACTIVITY:
            return {
                ...state,
                newActivity: action.payload
            }
        default: 
            return state;
    }
}