import { combineReducers } from 'redux';
import ActivityListReducer from './ActivityListReducer';

export default combineReducers({
    activities: ActivityListReducer
});